import React from 'react';

class Spinner extends React.Component {
    state = {showSpinner: true}

    componentDidMount () {
        if (this.props.hasTime) {
            setTimeout(() => this.setState({ showSpinner: false }), 5000)
        }
    }

    render(){
        return (
            this.state.showSpinner ? (
                <div>
                    <div className="ui active dimmer">
                        <div className="ui huge text loader">{this.props.message}</div>
                    </div>
                </div>
            ) :
            (
                this.props.children
            )
        );
    }
}

Spinner.defaultProps = {
    message: 'Loading ...'
}

export default Spinner;