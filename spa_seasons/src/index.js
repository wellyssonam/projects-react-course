import React from 'react';
import ReactDOM from 'react-dom';
import SeasonDisplay from './SeasonDisplay';
import Spinner from './Spinner';

// https://developers.google.com/web/fundamentals/native-hardware/user-location/?hl=pt-br
// http://www.linhadecodigo.com.br/artigo/3653/usando-geolocalizacao-com-html5.aspx

class App extends React.Component {
    state = {
        lat: -23.55052,
        errorMessage: '',
        hasTime: true
    }

    componentDidMount () {
        // check for Geolocation support
        /**
         * localização utilizando getCurrentLocation só é possível utilizando HTTPS
         */
        if (navigator.geolocation) {
            console.log('Geolocation is supported!')
            window.navigator.geolocation.getCurrentPosition(
                position => this.setState({ lat: position.coords.latitude }),
                err => {
                    console.log(err.message)
                    this.setState({ errorMessage: err.message })
                }
            );
        }
        else {
            console.log('Geolocation is not supported for this Browser/OS.');
        }
    }

    // componentDidUpdate () {
    //     console.log('My component was just update - it rerendered!');
    // }

    render () {
        if (this.state.errorMessage && !this.state.lat) {
            return <div>Error: {this.state.errorMessage}</div>;
        }

        if (!this.state.errorMessage && this.state.lat) {
            return <div>Latitude: {this.state.lat}</div>;
        }
        
        return (
            <Spinner hasTime={this.state.hasTime} message="Please accept location request">
                <SeasonDisplay lat={this.state.lat} />;
            </Spinner>
        );
        
    }
}

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
