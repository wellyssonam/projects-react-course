import { combineReducers } from 'redux';

const songsReducer = () => {
    return [
        { name: 'No Scrubs', duration: '4:05' },
        { name: 'Macarena', duration: '2:30' },
        { name: 'All Star', duration: '3:15' },
        { name: 'I Want It That Way', duration: '1:45' }
    ]
}

const selectSongReducer = (selectedSong=null, action) => {
    if (action.type === 'SONG_SELECTED') {
        return action.payload
    }
    return selectedSong
}

export default combineReducers({
    songs: songsReducer,
    selectedSong: selectSongReducer
})