# Wellysson | Front-End Developer | Projetos de curso em React Js 

## Etapas de Instalação e Execução

1 -  Primeiro certifique-se de que possui o node instalado para utilizar os comandos npm, para isso em seu terminal digite:
> npm -v

2 - Se o node não estiver instalado acesse o site para download (**[node](https://nodejs.org/en/download/)**)

3 - Após instalação o projeto já poderá ser executado, para isso localize e acesse o diretório do projeto pelo terminal, verifique se encontra-se na branch "master" e então execute o comando para a instalação de todas as dependência presentes no projeto.

Ex: Acessar diretório "songs"
> cd songs

Instalar dependências
> npm install

4 - No mesmo diretório digite o camando para executar o projeto.
> npm start
